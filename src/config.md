# Конфигурация
При первом запуске утилиты управления Amanita CLI:

1. Cоздается целевая папка, определяемая переменной окружения `AMANITA_PREFIX` (по умолчанию /srv/amanita). 
2. В данной папке создается файл `config.yml`, копия `config.default.yml` из пакета amanita_cli.

Далее в пути, указанном в `AMANITA_PREFIX`, будут созданы рабочие образы Odoo и других сервисов.

Если есть необходимость в переопределении `AMANITA_PREFIX`, это надо сделать до первого запуска утилиты
amanita cli, например, следующим образом:

```
AMANITA_PREFIX=/opt/amanita amanita
```
Далее содержимое `AMANITA_PREFIX` записывается в домашнюю директорию пользователя
в файл `~/.amanita`, и далее при работе с утилитой amanita указывать префикс не обязательно.


## amanita config
Не предполагается работы с `config.yml` напрямую, вместо этого предлагается использовать команду
`amanita config`:

```sh
$ amanita config --help
Usage: amanita config [OPTIONS] COMMAND [ARGS]...

  Configuration management

Options:
  --help  Show this message and exit.

Commands:
  del   Delete a configuration option.
  init  Initialize config.yml from defaults.
  set   Set a configuration option.
  show  Show configuration.

```

## Переопределение пути AMANITA_PREFIX
