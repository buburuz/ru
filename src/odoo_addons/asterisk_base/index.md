# Amanita Base
Основной и первичный модуль, обеспечивающий следующий базовый функционал:

* Управление серверами Asterisk.
* Управление конфигурационными файлами Asterisk с подсветкой синтаксиса.
* WEB консоль Asterisk (CLI).
* Безопасность - осуществляется мониторинг Security Events и блокировка на уровне файрвола.
* Активные каналы.
* Сконфигурированные екстеншены.
* Пользовательские диалпланы.

## Консоль Asterisk

![CLI](img/cli.png)

## Редактор .conf файлов

![CLI](img/extensions.conf.png)
